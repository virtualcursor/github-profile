import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import moment from "moment";

Vue.use(Vuex);

const axiosInstance = axios.create({
  baseURL: "https://api.github.com",
  auth: { username: process.env.VUE_APP_GITHUB_USERNAME },
  headers: { Accept: "application/vnd.github.v3+json" }
});

export default new Vuex.Store({
  state: {
    user: {},
    repositories: []
  },
  getters: {
    getUser: state => state.user,
    getRepositories: state => state.repositories,
    getRepository: state => id =>
      state.repositories.find(item => item.id === id)
  },
  mutations: {
    setUser: (state, user) => {
      state.user = user;
    },
    addRepository: (state, item) => {
      state.repositories.push(item);
    },
    updateRepository: (state, item) => {
      const repository = state.repositories.find(i => i.id === item.id);
      Object.assign(repository, item);
    }
  },
  actions: {
    fetchUser: async ({ commit }, user) => {
      try {
        const response = await axiosInstance.get(`/users/${user}`);

        commit("setUser", {
          avatarUrl: response.data.avatar_url,
          name: response.data.name,
          url: response.data.html_url,
          bio: response.data.bio,
          company: response.data.company,
          location: response.data.location,
          link: response.data.blog
        });
      } catch (error) {
        throw error;
      }
    },
    fetchRepositories: async ({ commit }, user) => {
      try {
        const response = await axiosInstance.get(
          `/search/repositories?q=user:${user}&sort=stars&order=desc`
        );

        response.data.items.map(item => {
          commit("addRepository", {
            id: item.id,
            full_name: item.full_name,
            url: item.html_url,
            description: item.description,
            homepage: item.homepage,
            stars: item.stargazers_count,
            forks: item.forks,
            language: item.language,
            owner: item.owner.login,
            name: item.name,
            events: []
          });
        });
      } catch (error) {
        throw error;
      }
    },
    fetchEvents: async ({ commit, getters }, itemID) => {
      try {
        const item = getters.getRepository(itemID);
        const response = await axiosInstance.get(
          `/repos/${item.owner}/${item.name}/events`
        );

        const events = response.data.map(event => ({
          id: event.id,
          actor: event.actor.display_login,
          type: event.type,
          created: moment(event.created_at).format("YYYY-MM-DD")
        }));

        item.events = events;

        commit("updateRepository", item);
      } catch (error) {
        throw error;
      }
    }
  }
});
